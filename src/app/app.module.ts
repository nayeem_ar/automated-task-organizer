import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { TaskOrganizerModule } from './main/task-organizer/task-organizer.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FlexLayoutModule } from '@angular/flex-layout';


const appRoutes: Routes = [

  {
    path: '',
    loadChildren: './main/task-organizer/task-organizer.module#TaskOrganizerModule',
  }
]


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    TaskOrganizerModule,
    BrowserAnimationsModule,
    FlexLayoutModule
  ],
  providers: [],
  exports: [
    FlexLayoutModule,
    AppComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
