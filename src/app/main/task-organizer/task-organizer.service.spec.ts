import { TestBed } from '@angular/core/testing';

import { TaskOrganizerService } from './task-organizer.service';

describe('TaskOrganizerService', () => {
  let service: TaskOrganizerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaskOrganizerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
