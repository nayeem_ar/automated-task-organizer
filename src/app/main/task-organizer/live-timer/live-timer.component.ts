import { Component, OnInit } from '@angular/core';
import { TaskOrganizerService } from '../task-organizer.service';

@Component({
  selector: 'app-live-timer',
  templateUrl: './live-timer.component.html',
  styleUrls: ['./live-timer.component.css'],
})
export class LiveTimerComponent implements OnInit {
  tasks: any[] = [];
  isTImerOn = false;

  times = [
    0, 10, 20, 30, 40, 50, 60
  ]

  constructor(private taskOrgService: TaskOrganizerService) { }

  ngOnInit(): void {
    this.taskOrgService.events$.forEach(event => this.tasks = JSON.parse(localStorage["tasks"]));
    if (localStorage.length === 0) {
    } else {
      this.tasks = JSON.parse(localStorage["tasks"]);
      this.tasks.sort(function(a, b) { 
        return a.task_duration - b.task_duration;
      });      
    }
  }

  animate({ timing, draw, duration }: any) {
    const start = performance.now();
    requestAnimationFrame(function animate(time) {
      let timeFraction = (time - start) / duration;
      if (timeFraction > 1) timeFraction = 1;
      let progress = timing(timeFraction);
      draw(progress); // draw it
      if (timeFraction < 1) requestAnimationFrame(animate);
    });
  }

  scroll() {
    this.isTImerOn = true;
    this.animate(
      {
        duration: 60000,
        timing: function (t: any) {
          return t;
        },
        draw: (pct: any) => {
          const air: any = document.querySelector('.air');
          const ball: any = document.querySelector('.ball');
          ball.style.bottom = pct * (air.offsetWidth - 20) + 'px';
        },
      }
    )
  }



}
