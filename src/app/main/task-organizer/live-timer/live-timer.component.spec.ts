import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveTimerComponent } from './live-timer.component';

describe('LiveTimerComponent', () => {
  let component: LiveTimerComponent;
  let fixture: ComponentFixture<LiveTimerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveTimerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveTimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
