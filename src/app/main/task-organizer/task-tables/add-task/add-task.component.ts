import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
})
export class AddTaskComponent implements OnInit {
  form!: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<AddTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.form = this._fb.group({
      id: [''],
      task_name: ['', Validators.required],
      task_duration: [, Validators.required],
    });
    this.setFormData();
  }

  save(): void {
    this._dialogRef.close(this.form.value);
  }

  close(): void {
    this._dialogRef.close();
  }

  private setFormData(): void {
    if (this.data) {
      this.form.patchValue(this.data.tasks);
    }
  }
}
