import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';
import { TaskOrganizerService } from '../task-organizer.service';
import { AddTaskComponent } from './add-task/add-task.component';

export interface TaskData {
  id: string;
  task_name: string;
  task_duration: number;
}

@Component({
  selector: 'app-task-tables',
  templateUrl: './task-tables.component.html',
  styleUrls: ['./task-tables.component.css'],
})

export class TaskTablesComponent implements OnInit {
  tasks: any[] = []
  constructor(public dialog: MatDialog, private taskOrgService: TaskOrganizerService) { }

  ngOnInit(): void {
    if (localStorage.length !== 0) {
      this.tasks = JSON.parse(localStorage["tasks"]);
      if (!this.tasks) {
        localStorage["tasks"] = JSON.stringify(this.tasks);
      }
    }
  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '400px';

    const dialogRef = this.dialog.open(AddTaskComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        this.tasks.push({ ...data, id: this.randomId() });
        localStorage["tasks"] = JSON.stringify(this.tasks);
        this.taskOrgService.newEvent('clicked!');
      }
    });
  }

  randomId() {
    return Math.random().toString(36).slice(2);;
  }

  onEdit(row: TaskData): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = { tasks: row };
    dialogConfig.width = '400px';

    const dialogRef = this.dialog.open(AddTaskComponent, dialogConfig);

    this.updateTask(dialogRef);
  }

  updateTask(dialogRef: MatDialogRef<AddTaskComponent, any>): void {
    dialogRef.afterClosed().subscribe((data) => {
      this.tasks.forEach(item => {
        if (item.id === data.id) {
          item.task_name = data.task_name;
          item.task_duration = data.task_duration;
          localStorage["tasks"] = JSON.stringify(this.tasks);
          this.taskOrgService.newEvent('clicked!');
        }
      })
    });
  }

  onDelete(task: TaskData): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirm Remove Task',
        message: 'Are you sure you want to delete?'
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.tasks = this.tasks.filter(item => item.id !== task.id);
        localStorage["tasks"] = JSON.stringify(this.tasks);
        this.taskOrgService.newEvent('clicked!');

      }
    });
  }
}
