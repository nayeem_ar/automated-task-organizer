import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskTablesComponent } from './task-tables/task-tables.component';
import { LiveTimerComponent } from './live-timer/live-timer.component';
import { RouterModule, Routes } from '@angular/router';
import { TaskOrganizerComponent } from './task-organizer.component';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { AddTaskComponent } from './task-tables/add-task/add-task.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';

const routes: Routes = [
  {
    path: '',
    component: TaskOrganizerComponent,
  },
];

@NgModule({
  declarations: [
    TaskTablesComponent,
    LiveTimerComponent,
    TaskOrganizerComponent,
    AddTaskComponent,
    ConfirmDialogComponent,
  ],
  entryComponents: [AddTaskComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    FlexLayoutModule
  ],
})
export class TaskOrganizerModule { }
